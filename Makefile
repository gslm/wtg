
CC = g++
CFLAGS = -std=c++14
OPT = -O2
DFLAGS =

BUILD = build
INCLUDE = -I../include
LIB = -L../lib
SRC = ../src/

LIB_LIST = -lpthread -lOpenImageIO

SOURCES = WangTileGenArgs.cpp WangTileBorderSet.cpp WangTileGen.cpp main.cpp
OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = ../WangTileGen

all: create_build_folder $(EXECUTABLE)

create_build_folder:
	mkdir -p $(BUILD)

$(EXECUTABLE): $(OBJECTS)
	cd $(BUILD) && $(CC) $(CFLAGS) $(OPT) $(DFLAGS) $(OBJECTS) $(LIB) $(LIB_LIST) -o $@

$(OBJECTS):
	cd $(BUILD) && $(CC) $(CFLAGS) $(OPT) $(DFLAGS) -c $(SRC)$(@:.o=.cpp) $(INCLUDE)

clean:
	rm -rf $(BUILD) 
