# Wang Tile Atlas Generator

Wang tile atlas generator is an application created to synthesize Wang tile
atlases from a source image. The atlas is arranged following the layout
described in *Li-Yi Wei. “Tile-based Texture Mapping on Graphics Hardware”* and
a variation on the technique described in *Michael F. Cohen et al. “Wang Tiles
for Image and Texture Generation”* is used to generate the tile borders.  In
order to get more variation on the tile content the synthesis algorithm
described in *Alexei A. Efros i William T. Freeman. “Image Quilting for Texture
Synthesis and Transfer”* is used to fill ythe tile's core.

## Usage
```
WangTileGen options
```
### Options

* **-i** input\_file
  
  Required. Source image file path from where the Wang tile set
  will be synthesized.

* **-t** tile\_size
  
  Required. Size in pixels of the output tiles.

* **-b** border\_size

  Required. Size in pixels of the tile borders.

* **-p** border\_overlap

  Required. Size in pixels of the overlap between the samples composing the
  borders. 

* **-v** vertical\_tile\_count

  Required. Number of tiles in the vertical coordinate. 

* **-h** horizontal\_tile\_count

  Required. Number of tiles in the horizontal coordinate.

* **-d** fill\_density

  Required. Number of patches used to synthesize the content of a tile.

* **-c** border\_budget

  Optional. Iteration count allowed to find the best tile set possible. 

* **-f** fill\_budged

  Optional. Iteration count allowed to find the best patch when synthesising
  tile content.

* **-s** seed

  Optional. Random seed used.

* **-o** output\_file
  
  Optional. Output image file. In not specified a naming based on the input
  image name is used.

## Build

Run make in the project's root directory.
```
$ make
```

### Tools

* make.
* g++. Any version that fully supports c++14.

### Dependencies

* OpenImageIO
* GLM

## Implementation Overview

The Atlas synthesis is split in 3 stages. All operation are done in place in the
output image using the arrangement scheme described in *Li-Yi Wei. “Tile-based
Texture Mapping on Graphics Hardware”* 

1. First, a set of source tiles is selected. N sets of tiles are selected
   randomly and a score describing its quality is computed for each.  The
   best scoring set is kept for the next stage.

2. Second, The tile borders are assembled using the method described in *Michael
   F. Cohen et al. “Wang Tiles for Image and Texture Generation”*.

3. Finally, The tile's core is synthesized using the algorithm in *Alexei A.
   Efros i William T. Freeman. “Image Quilting for Texture Synthesis and
   Transfer”*.
