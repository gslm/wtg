#ifndef IMAGE_QUILT_HPP
#define IMAGE_QUILT_HPP

#include <type_traits>
#include <vector>

namespace quilt {

template<class T>
class Matrix {
    std::size_t width_;
    std::size_t height_;

    std::vector<T> data_;

public:
    Matrix() {
        width_ = data_.size();
        height_ = 0;
    }

    Matrix(size_t height, size_t width, const T& value = T()) {
        width_ = width;
        height_ = height;
        data_ = std::vector<T>(width*height, value);
    }

    ~Matrix() {}

    std::size_t width() const { return width_; }
    std::size_t height() const { return height_; }
    const std::vector<T>& data() const { return data_; }
    std::vector<T>& data() { return data_; }

    T& operator()(std::size_t i, std::size_t j) {
        assert(i < height_);
        assert(j < width_);

        return data_[width_*i + j];
    }
    
    const T& operator()(std::size_t i, std::size_t j) const {
        assert(i < height_);
        assert(j < width_);

        return data_[width_*i + j]; 
    }     
};

template<class T, class Fn>
void fill_error_table(Matrix<T>& error_table, const Fn& compute_error) {
    //static_assert(std::is_convertible<std::result_of_t<Fn>,T>::value, "");

    for (int j = 0; j < error_table.width(); ++j) {
        error_table(0,j) = compute_error(0,j);
    }

    for (int i = 1; i < error_table.height(); ++i) {
        for (int j = 0; j < error_table.width(); ++j) {	
            error_table(i,j) = error_table(i-1,j);

            if (j > 0) {
                error_table(i,j) = std::min(error_table(i,j), error_table(i-1,j-1));
            }

            if (j < error_table.width() - 1) {
                error_table(i,j) = std::min(error_table(i,j), error_table(i-1,j+1));
            }

            error_table(i,j) += compute_error(i, j);
        }
    }
}

template<class T>
size_t get_min_error_index(const Matrix<T>& error_table) {
    size_t min_index = 0;
    for (size_t j = 1; j < error_table.width(); ++j) {
        if (error_table(error_table.height() - 1, j) < 
            error_table(error_table.height() - 1, min_index)) {
            min_index = j;
        }
    }

    return min_index;
}

template<class T>
T get_min_error(const Matrix<T>& error_table) {
    return error_table(error_table.height() - 1, 
                       get_min_error_index(error_table));
}

template<class T>
T trace_path(const Matrix<T>& error_table, std::vector<int>& path) {

    path[path.size() - 1] = get_min_error_index(error_table);

    for (int i = error_table.height() - 2; i >= 0; --i) {
        path[i] = path[i+1];
    
        if (path[i+1] > 0) {
            if (error_table(i,path[i+1] - 1) < error_table(i,path[i])) { 
                path[i] = path[i+1] - 1;
            }
        }

        if (path[i+1] < error_table.width() - 1) {
            if (error_table(i,path[i+1] + 1) < error_table(i,path[i])) {
                path[i] = path[i+1] + 1;
            }
        }	
    }

    return error_table(error_table.height() - 1, path.back());
}

/*
template<class Fn1, class Fn2, class Fn3>
void apply_path(const std::vector<int>& path, int overlap, 
                Fn1 fetch_pixel1, Fn2 fetch_pixel2, Fn3 set_pixel) {
    for (auto&& i : path) {
        for (int j = 0; j < overlap; ++j) {
            if (i < j) set_pixel(i, j, fetch_pixel1(i, j));
            else set_pixel(i, j, fetch_pixel2(i, j));
        }
    }
}
*/
}

#endif
