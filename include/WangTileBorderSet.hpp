#ifndef WANG_TILE_BORDER_CHOOSER_HPP
#define WANG_TILE_BORDER_CHOOSER_HPP

#include <glm/glm.hpp>
#include <vector>

#include <WangTileGen.hpp>

namespace wtg {

class WangTileBorderSet {
    int Kv_;
    int Kh_;   
 
    Image* src_img_;
    std::vector<glm::ivec2> samples_[2];

    std::vector<glm::ivec2>* samples_ptr_;
    std::vector<glm::ivec2>* best_samples_ptr_;

    int sets_tested_;

public:
    WangTileBorderSet(Image* src_img, int Kv, int Kh);
    ~WangTileBorderSet();

    void GenerateSet(int tile_size, int overlap, int border, 
                     int budget, int seed = 0);

    glm::ivec2 vertical(int v) const;
    glm::ivec2 horizontal(int h) const;

    int sets_tested() const;
};

}

#endif
