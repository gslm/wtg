#ifndef WANG_TILE_GEN_HPP
#define WANG_TILE_GEN_HPP

#include <array>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>

#include <ImageQuilt.hpp>

namespace wtg {

template<class T>
using Matrix = quilt::Matrix<T>;

struct Image : public Matrix<glm::u8vec4> {
    using pixel_t = glm::u8vec4;  
    Image() {}
    Image(size_t height, size_t width, 
          const pixel_t& value = pixel_t(255, 0, 255, 255)) 
        : Matrix<pixel_t>(height, width, value) {} 
};

int border_sample_size(int tile_size, int overlap);

int error_table_height(int overlap, int border);

int compute_tile_error(const Image& src_img,
                       const glm::ivec2& st, const glm::ivec2& sb, 
                       const glm::ivec2& sl, const glm::ivec2& sr,
                       int tile_size, int overlap, int border_size, 
                       Matrix<int>& error_table);

void set_tile_borders(const Image& src_img, 
                      const glm::ivec2& st, const glm::ivec2& sb,
                      const glm::ivec2& sl, const glm::ivec2& sr,
                      int tile_size, int overlap, int border_size,
                      Image& tile_pack, const glm::ivec2& pack_offset,
                      Matrix<int>& error_table, 
                      std::array<std::vector<int>, 4>& paths);

int fill_overlap(int border_size);

int fill_sample_size(int fill_density, int tile_size, int border_size); 

void fill_tile(const Image& src_img, 
               Image& tile_pack, const glm::ivec2& pack_offset, 
               int fill_density, int fill_budget, int seed,
               Matrix<int>& error_matrix, 
               std::array<std::vector<int>, 4>& paths);

glm::ivec2 tile_pack_index(int t, int b, int l, int r); 

}

#endif
