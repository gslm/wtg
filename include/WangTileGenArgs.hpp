#ifndef WANG_TILE_GEN_ARGS_HPP
#define WANG_TILE_GEN_ARGS_HPP

#include <string>

namespace wtg {

class WangTileGenArgs {

    std::string input_;
    std::string output_;

    int tile_size_;

    int border_size_;
    int border_budget_;
    int border_overlap_;

    int fill_density_;
    int fill_budget_;

    int h_;
    int v_;

    int seed_;
    int quality_;

    int error_code_;

public:
    WangTileGenArgs();
    ~WangTileGenArgs();

    bool Parse(int argc, char** argv);

    int error_code() const;
    std::string input() const;
    std::string output() const;

    int tile_size() const;

    int border_size() const;
    int border_budget() const;
    int border_overlap() const;

    int fill_density() const;
    int fill_budget() const;

    int h() const;
    int v() const;
    int seed() const;

};

}


#endif
