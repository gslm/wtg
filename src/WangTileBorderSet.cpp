#include <WangTileBorderSet.hpp>

#include <limits>
#include <random>
#include <cassert>

namespace wtg {

WangTileBorderSet::WangTileBorderSet(Image* src_img, int Kv, int Kh) {
    src_img_ = src_img;    

    Kv_ = Kv;
    Kh_ = Kh;

    samples_[0] = std::vector<glm::ivec2>(Kv+Kh);
    samples_[1] = std::vector<glm::ivec2>(Kv+Kh);
};

WangTileBorderSet::~WangTileBorderSet() {}

void WangTileBorderSet::GenerateSet(int tile_size, int overlap, int border, 
                                    int budget, int seed) { 
    samples_ptr_ = &samples_[0];
    best_samples_ptr_ = &samples_[1];

    int sample_size = border_sample_size(tile_size, overlap); 

    long int best_error = std::numeric_limits<long int>::max();

    std::default_random_engine gen(seed);
    const int offset = 5;
    const int b = int(glm::ceil(glm::one_over_root_two<float>() * 
                                float(sample_size))) + offset;

    std::uniform_int_distribution<int> xdis(offset, src_img_->width() - b*2 );
    std::uniform_int_distribution<int> ydis(b, src_img_->height() - b);
    
    auto generate_random_set = [&]() {
        for (int v = 0; v < Kv_; ++v) {
            (*samples_ptr_)[v] = glm::ivec2(xdis(gen), ydis(gen));
        }

        for (int h = 0; h < Kh_; ++h) {
            (*samples_ptr_)[Kv_ + h] = glm::ivec2(xdis(gen), ydis(gen));
        }
    }; 

    Matrix<int> error_table(error_table_height(overlap, border), overlap);
    auto compute_set_error = [&]() {
        long int error = 0;

        for (int t = 0; t < Kv_; ++t) {
            for (int b = 0; b < Kv_; ++b) {
                for (int l = 0; l < Kh_; ++l) {
                    for (int r = 0; r < Kh_; ++r) {
                        error += compute_tile_error(*src_img_,
                                                    (*samples_ptr_)[t],
                                                    (*samples_ptr_)[b], 
                                                    (*samples_ptr_)[Kv_ + l], 
                                                    (*samples_ptr_)[Kv_ + r],
                                                    tile_size, overlap, border,
                                                    error_table); 
                    }
                }
            }
        }

        return error;
    }; 


    sets_tested_ = 0;
    for (int i = 0; i < budget; ++i) { 
        generate_random_set();
        long int set_error = compute_set_error();

        if (best_error > set_error) {
            best_error = set_error;
            std::swap(best_samples_ptr_, samples_ptr_);
        }

        ++sets_tested_;
    }
}

glm::ivec2 WangTileBorderSet::vertical(int v) const {
    assert(v < Kv_);
    return (*best_samples_ptr_)[v];
}

glm::ivec2 WangTileBorderSet::horizontal(int h) const { 
    assert(h < Kh_);
    return (*best_samples_ptr_)[Kv_ + h]; 
}

int WangTileBorderSet::sets_tested() const {
    return sets_tested_;
}

}
