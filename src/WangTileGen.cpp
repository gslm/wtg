#include <WangTileGen.hpp>

#include <random>

namespace {
    const glm::mat2 rot0  (1.0f, 0.0f, 0.0f, 1.0f);
    const glm::mat2 rot90 (0.0f, 1.0f, -1.0f, 0.0f);
    const glm::mat2 rot180(-1.0f, 0.0f, 0.0f, -1.0f);
    const glm::mat2 rot270(0.0f, -1.0f, 1.0f, 0.0f);

    const glm::mat2 rot45(glm::one_over_root_two<float>(),
                           glm::one_over_root_two<float>(),
                           -glm::one_over_root_two<float>(),
                            glm::one_over_root_two<float>());

    int error(const wtg::Image& img1, const wtg::Image& img2,
              const glm::vec2& p1, const glm::vec2& p2,
              auto sampler) {
        glm::ivec4 cv = sampler(img2, p2) - sampler(img1, p1);
        return cv.r*cv.r + cv.g*cv.g + cv.b*cv.b + cv.a*cv.a;  
    }

    wtg::Image::pixel_t linear_sampler(const wtg::Image& img, const glm::vec2& p) {
        glm::vec2 img_max = glm::vec2(img.width() - 1, img.height() - 1); 

        glm::vec2 a = glm::floor(p);
        glm::vec2 b = glm::min(a + glm::vec2(1.0, 0.0), img_max);
        glm::vec2 c = glm::min(a + glm::vec2(0.0, 1.0), img_max);
        glm::vec2 d = glm::min(a + glm::vec2(1.0, 1.0), img_max);

        glm::vec4 ca = img(a.y,a.x);
        glm::vec4 cb = img(b.y,b.x);
        glm::vec4 cc = img(c.y,c.x);
        glm::vec4 cd = img(d.y,d.x);

        glm::vec2 s = p - a; //glm::fract(p);

        glm::vec4 cab = glm::mix(ca, cb, s.x);
        glm::vec4 ccd = glm::mix(cc, cd, s.x);
        
        glm::vec4 r = glm::round(glm::mix(cab, ccd, s.y));
        return wtg::Image::pixel_t(r);
    };

    wtg::Image::pixel_t nearest_sampler(const wtg::Image& img, const glm::vec2& p) {
        glm::vec2 a = glm::round(p);
        return img(a.y, a.x);
    }

    int compute_border_error(const wtg::Image& src_img,
                             const glm::mat2& rot, const glm::vec2& t, 
                             const glm::vec2& t1, const glm::vec2& t2,
                             const glm::vec2& s1, const glm::vec2& s2,
                             const glm::vec2& p) {
        glm::vec2 tp = (rot * (p + t)) * rot45;

        glm::vec2 p1 = (tp - t1) + s1;
        glm::vec2 p2 = (tp - t2) + s2;

        return error(src_img, src_img, p1, p2, linear_sampler);
    }

}

namespace wtg {

int border_sample_size(int tile_size, int overlap) {
    return glm::floor((float(tile_size) + 
                      float(overlap) * glm::root_two<float>()) * 
                      glm::one_over_root_two<float>());
}

int error_table_height(int overlap, int border) {
    return glm::ceil(glm::root_two<float>()*float(border) + 
                     float(overlap));
}

int compute_tile_error(const Image& src_img,
                       const glm::ivec2& st, const glm::ivec2& sb, 
                       const glm::ivec2& sl, const glm::ivec2& sr,
                       int tile_size, int overlap, int border_size, 
                       Matrix<int>& error_table) {

    int sample_size = border_sample_size(tile_size, overlap);
    
    auto compute_path_error = [&](auto compute_error) -> int {
        quilt::fill_error_table(error_table, compute_error);
        return quilt::get_min_error(error_table);
    };

    const glm::vec2 tt(-glm::one_over_root_two<float>() * 
                       float(sample_size - overlap));
    const glm::vec2 tb(tt.x, -tt.y);
    const glm::vec2 tl(-glm::one_over_root_two<float>() * 
                        float(2*sample_size - overlap), 0.0f);
    const glm::vec2 tr(-glm::one_over_root_two<float>() * 
                        float(overlap), 0.0f);

    auto compute_error = [&](const glm::mat2& rot,
                             const glm::vec2& t1, const glm::vec2& t2,
                             const glm::vec2& s1, const glm::vec2& s2,
                             const glm::vec2& p) {
        const glm::vec2 t(-float(overlap - 1)*0.5, -(float(error_table.height() - 1) + tr.x));
        return ::compute_border_error(src_img, rot, t, t1, t2, s1, s2, p);
    };

    int error = 0;
    error += compute_path_error(
        [&](int i, int j) -> int {
            return compute_error(::rot90, tt, tr, st, sr, glm::vec2(j, i)); 
        });

    error += compute_path_error(
        [&](int i, int j) -> int {
            return compute_error(::rot180, tr, tb, sr, sb, glm::vec2(j, i)); 
        });
        
    error += compute_path_error(
        [&](int i, int j) -> int { 
            return compute_error(::rot270, tb, tl, sb, sl, glm::vec2(j, i));
        });

    error += compute_path_error(
        [&](int i, int j) -> int { 
            return compute_error(::rot0, tl, tt, sl, st, glm::vec2(j, i));
        });
   
    return error;
}

void set_tile_borders(const Image& src_img, 
                      const glm::ivec2& st, const glm::ivec2& sb,
                      const glm::ivec2& sl, const glm::ivec2& sr,
                      int tile_size, int overlap, int border_size,
                      Image& tile_pack, const glm::ivec2& pack_offset,
                      Matrix<int>& error_table, 
                      std::array<std::vector<int>, 4>& paths) {

    const int sample_size = border_sample_size(tile_size, overlap);

    auto compute_path = [&](auto compute_error, std::vector<int>& path) {
        quilt::fill_error_table(error_table, compute_error);
        quilt::trace_path(error_table, path);    
    };

    const glm::vec2 tt(-glm::one_over_root_two<float>() *
                                    float(sample_size - overlap));
    const glm::vec2 tb(tt.x, -tt.y);
    const glm::vec2 tl(-glm::one_over_root_two<float>() *
                                    float(2*sample_size - overlap), 0.0f);
    const glm::vec2 tr(-glm::one_over_root_two<float>() * 
                                    float(overlap), 0.0f);

    auto compute_error = [&](const glm::mat2& rot,
                             const glm::vec2& t1, const glm::vec2& t2,
                             const glm::vec2& s1, const glm::vec2& s2,
                             const glm::vec2& p) {
        const glm::vec2 t(-float(overlap - 1)*0.5f , -(float(error_table.height() - 1) + tr.x));
        return ::compute_border_error(src_img, rot, t, t1, t2, s1, s2, p);
    };

    enum : int { TR = 0, LT = 1, BL = 2, RB = 3 };    
   
    compute_path(
        [&](int i, int j) -> int { 
            return compute_error(::rot90, tt, tr, st, sr, glm::vec2(j, i)); 
        }, paths[TR]);

    compute_path(
        [&](int i, int j) -> int { 
            return compute_error(::rot180, tr, tb, sr, sb, glm::vec2(j, i)); 
        }, paths[LT]);
    
    compute_path(
        [&](int i, int j) -> int { 
            return compute_error(::rot270, tb, tl, sb, sl, glm::vec2(j, i)); 
        }, paths[BL]);        

    compute_path(
        [&](int i, int j) -> int { 
            return compute_error(::rot0, tl, tt, sl, st, glm::vec2(j, i));
        }, paths[RB]);  

    auto set_tile_pixel = [&](const glm::mat2& rot, 
                              const glm::vec2& t1, const glm::vec2& t2,
                              const glm::vec2& s1, const glm::vec2& s2,
                              const glm::vec2& p, 
                              const std::vector<int>& path) -> void {

        const glm::vec2 t(-float(tile_size - 1)*0.5f);
        glm::vec2 tp = rot * (p + t);
        glm::ivec2 des_p = pack_offset + glm::ivec2(tp - t); 
        
        glm::vec2 ttp = ::rot45 * (p);

        const float o = float(overlap);
        int overlap_test = int(ttp.x > o*0.5f) - int(ttp.x < -o*0.5f);

        if (overlap_test == 0) {
            glm::ivec2 iotp = glm::round(ttp + glm::vec2(o*0.5f, 0.0f));
           
            if (path[iotp.y] > iotp.x) {
                tile_pack(des_p.y,des_p.x) = ::nearest_sampler(src_img, (tp - t1) + s1);
            }
            else tile_pack(des_p.y,des_p.x) = ::nearest_sampler(src_img, (tp - t2) + s2);
        }
        else if (overlap_test < 0) { 
            tile_pack(des_p.y,des_p.x) = ::nearest_sampler(src_img, (tp - t1) + s1);
        }
        else tile_pack(des_p.y,des_p.x) = ::nearest_sampler(src_img, (tp - t2) + s2);
    };

    auto compose_corner = [&](const glm::mat2& rot, 
                              const glm::vec2& t1, const glm::vec2& t2,
                              const glm::vec2& s1, const glm::vec2& s2,
                              const std::vector<int>& path) -> void {
        for (int i = 0; i < border_size; ++i) {
            for (int j = 0; j < tile_size/2 + 1; ++j) {
                set_tile_pixel(rot, t1, t2, s1, s2, glm::vec2(j, i), path); 
            }
        }

        for (int i = border_size; i < tile_size/2 + 1; ++i) {
            for (int j = 0; j < border_size; ++j) { 
                set_tile_pixel(rot, t1, t2, s1, s2, glm::vec2(j, i), path);
            }
        }
    };

    compose_corner(::rot0, tl, tt, sl, st, paths[LT]);
    compose_corner(::rot90, tt, tr, st, sr, paths[TR]);
    compose_corner(::rot180, tr, tb, sr, sb, paths[RB]);
    compose_corner(::rot270, tb, tl, sb, sl, paths[BL]);
}

int fill_overlap(int border_size) {
    return border_size/2 + border_size%2;
}

int fill_sample_size(int fill_density, int tile_size, int border_size) {
    return 1 + (fill_overlap(border_size)*(fill_density - 3) + tile_size) / 
                fill_density;
}

void fill_tile(const Image& src_img, 
               Image& tile_pack, const glm::ivec2& pack_offset, 
               int fill_density, int fill_budget, int seed,
               Matrix<int>& error_matrix, 
               std::array<std::vector<int>, 4>& paths) {

    seed += pack_offset.x + pack_offset.y*fill_density;

    std::default_random_engine gen(seed);
    std::uniform_int_distribution<int> dis_x
            (1, src_img.width() - error_matrix.height() - 1);
    std::uniform_int_distribution<int> dis_y
            (1, src_img.height() - error_matrix.height() - 1);
    
    const int overlap = error_matrix.width();
    const int sample_size = error_matrix.height();

    auto fill_sample = [&](int i, int j) -> glm::ivec2 {
        return glm::ivec2(j, i)*(sample_size - overlap) + overlap;
    }; 

    auto find_sample = [&](int i, int j) -> glm::ivec2 {
        const glm::ivec2 p = fill_sample(i, j) + pack_offset;

        auto next_sample = [&, sc = 0, current_sample = glm::ivec2(0)]
                           (glm::ivec2& sample) mutable -> bool {
            if (sc > fill_budget) {
                sc = 0;
                return false;
            }

            const int local_samples = 16; 
            if (sc % local_samples == 0) current_sample = glm::ivec2(dis_x(gen), dis_y(gen));
            current_sample.x = (current_sample.x + sample_size) %
                               (src_img.width() - sample_size);
            ++sc;

            sample = current_sample;
            return true;
        };

        auto compute_path_error = [&](auto compute_error) -> int {
            quilt::fill_error_table(error_matrix, compute_error);
            return quilt::get_min_error(error_matrix);
        };
 
        int min_cost = std::numeric_limits<int>::max();

        glm::ivec2 s, best_sample;
        while (next_sample(s)) {
            int v_sample_cost = compute_path_error([&](int i, int j) { 
                                    return  ::error(tile_pack, src_img,
                                                          p + glm::ivec2(i, j), 
                                                          s + glm::ivec2(i, j),
                                                          ::nearest_sampler);
                                });
            int h_sample_cost = compute_path_error([&](int i, int j) { 
                                    return ::error(tile_pack, src_img,
                                                         p + glm::ivec2(j, i),
                                                         s + glm::ivec2(j, i),
                                                         ::nearest_sampler);
                                });
            
            if (min_cost > v_sample_cost + h_sample_cost) {
                min_cost = v_sample_cost + h_sample_cost;

                best_sample = s;
            }
        }

        return best_sample;
    };
    
    auto apply_sample = [&](int i, int j, const glm::ivec2& s) -> void {

        auto compute_path = [&](auto compute_error, 
                                std::vector<int>& path) -> void {
            quilt::fill_error_table(error_matrix, compute_error);
            quilt::trace_path(error_matrix, path);
        };

        auto apply_center = [&](auto set) {
            for (int i = overlap; i < sample_size - overlap; ++i) {
                for (int j = overlap; j < sample_size - overlap; ++j) {
                    set(i, j);
                }
            }
        };

        auto apply_side = [&](auto set_l, auto set_r, 
                              const std::vector<int>& path) {
            for (int i = overlap; i < sample_size - overlap; ++i) {
                for (int j = 0; j < overlap; ++j) {
                    //if (path[i] == j) set(i, j, color); else
                    if (path[i] < j) set_l(i, j);
                    else set_r(i, j);
                }
            }
        };

        auto apply_corner = [&](const glm::ivec2& corner,
                                auto set_tl, auto set_tr,
                                auto set_bl, auto set_br,
                                const std::vector<int>& v_path, 
                                const std::vector<int>& h_path) -> void {
            const glm::ivec2 offset = corner * (sample_size - overlap);
            for (int i = 0; i < overlap; ++i) {
                for (int j = 0; j < overlap; ++j) {
                    if (j < v_path[i + offset.y]) {
                        if (i < h_path[j+offset.x]) set_tl(i+offset.y, j);
                        else set_bl(i+offset.y, j);
                    }
                    else {
                        if (i < h_path[j + offset.x]) set_tr(i+offset.y, j);
                        else set_br(i+offset.y, j);
                    } 
                }
            } 
        };

        glm::ivec2 p = fill_sample(i, j) + pack_offset;
        auto do_nothing = [](int i, int j) {};        
 
        auto from_sample_left = [&](int i, int j) {
            tile_pack(p.y+i, p.x+j) = src_img(s.y+i, s.x+j);
        };
        
        auto from_sample_top = [&](int i, int j) {
            tile_pack(p.y+j, p.x+i) = src_img(s.y+j, s.x+i);
        }; 

        auto from_sample_right = [&](int i, int j) {
            const int offset = sample_size - overlap ;
            tile_pack(p.y+i, p.x+j+offset) = src_img(s.y+i, s.x+j+offset); 
        };

        auto from_sample_bottom = [&](int i, int j) {
            const int offset = sample_size - overlap;
            tile_pack(p.y+j+offset, p.x+i) = src_img(s.y+j+offset, s.x+i);
        };

        const glm::ivec2 corner_tl(0, 0);
        const glm::ivec2 corner_tr(1, 0);
        const glm::ivec2 corner_bl(0, 1);
        const glm::ivec2 corner_br(1, 1);

        apply_center(from_sample_left);

        compute_path([&](int i, int j) {
            return ::error(tile_pack, src_img, 
                                 p + glm::ivec2(i, j),
                                 s + glm::ivec2(i, j),
                                 ::nearest_sampler);
        }, paths[0]);

        compute_path([&](int i, int j) {
            return ::error(tile_pack, src_img, 
                                 p + glm::ivec2(j, i),
                                 s + glm::ivec2(j, i),
                                 ::nearest_sampler);
        }, paths[1]);

        apply_corner(corner_tl, 
                     do_nothing, do_nothing, 
                     do_nothing, from_sample_left, 
                     paths[0], paths[1]);

        apply_side(from_sample_left, do_nothing, paths[0]);
        apply_side(from_sample_top, do_nothing, paths[1]);

        if (j < (fill_density - 1)) {
            apply_corner(corner_tr, 
                         do_nothing, do_nothing, 
                         from_sample_right, from_sample_right, 
                         paths[1], paths[1]);
            apply_side(from_sample_right, from_sample_right, paths[0]);
        }
        else {
            compute_path([&](int i, int j) {
                const int offset = sample_size - overlap;
                return ::error(tile_pack, src_img, 
                                     p + glm::ivec2(j+offset, i),
                                     s + glm::ivec2(j+offset, i),
                                     ::nearest_sampler);
            }, paths[2]);

            apply_corner(corner_tr, 
                         do_nothing, do_nothing, 
                         from_sample_right, do_nothing,
                         paths[0], paths[2]);
            apply_side(do_nothing, from_sample_right, paths[2]);
        }

        if (i < (fill_density - 1)) {
            apply_corner(corner_bl, 
                         do_nothing, from_sample_left, 
                         do_nothing, from_sample_left,
                         paths[0], paths[2]);
            apply_side(from_sample_bottom, from_sample_bottom, paths[0]);
        }
        else {
            compute_path([&](int i, int j) {
                const int offset = sample_size - overlap;
                return ::error(tile_pack, src_img, 
                                     p + glm::ivec2(i, j+offset),
                                     s + glm::ivec2(i, j+offset),
                                     ::nearest_sampler);
            }, paths[3]);

            apply_corner(corner_bl, 
                         do_nothing, from_sample_left, 
                         do_nothing, do_nothing, 
                         paths[0], paths[3]); 
            apply_side(do_nothing, from_sample_bottom, paths[3]);
        }

        if (i < (fill_density - 1) and j < (fill_density - 1)) {
            apply_corner(corner_br, from_sample_right, from_sample_right, 
                                    from_sample_right, from_sample_right,
                         paths[0], paths[0]);
        }
        else {
            apply_corner(corner_br, 
                         from_sample_right, do_nothing, do_nothing, do_nothing, 
                         paths[2], paths[3]);
        }
    };



    for (int i = 0; i < fill_density; ++i) {
        for (int j = 0; j < fill_density; ++j) {
            apply_sample(i, j, find_sample(i, j));
        }
    }

}

glm::ivec2 tile_pack_index(int t, int b, int l, int r) {
    auto tile_index_1d = [](const glm::ivec2& e) -> int {
        int c0 =  0 * int(e.x == e.y && e.y == 0);
        int c1 = (e.x * e.x + 2 * e.y - 1) * int(e.x > e.y && e.y > 0);
        int c2 = (2 * e.x + e.y * e.y) * int(e.y > e.x && e.x >= 0);
        int c3 = ((e.x + 1) * (e.x + 1) - 2) * int(e.x == e.y && e.y > 0);
        int c4 = ((e.x + 1) * (e.x + 1) - 1) * int(e.x > e.y && e.y == 0);

        return c0 + c1 + c2 + c3 + c4;        
    };

    auto tile_index_2d = [&](const glm::ivec4& tile_edges) {
        return glm::ivec2(tile_index_1d(glm::ivec2(tile_edges.z, 
                                                   tile_edges.w)), 
                          tile_index_1d(glm::ivec2(tile_edges.x, 
                                                   tile_edges.y)));
    }; 

    return tile_index_2d(glm::ivec4(t, b, l, r));
}


}
