#include <WangTileGenArgs.hpp>

#include <algorithm>
#include <functional>

namespace wtg {

WangTileGenArgs::WangTileGenArgs() {
    error_code_ = -1;
}

WangTileGenArgs::~WangTileGenArgs() {}

bool WangTileGenArgs::Parse(int argc, char** argv) {
    enum ParseState : int { START = 0, START_OP, 
                            OP_I, OP_O, OP_T, OP_B, OP_P, OP_C, OP_D, 
                            OP_F, OP_H, OP_V, OP_S, OP_Q, 
                            END, ERROR};

    const int DEFAULTS_SIZE = 7;
    int DEFAULT_BORDER_BUDGET[DEFAULTS_SIZE] = {32, 64, 128, 256, 512, 1024, 2048};
    int DEFAULT_FILL_BUDGET[DEFAULTS_SIZE] = {32, 64, 128, 256, 512, 1024, 2048};

    auto init_vars = [&]() -> void {
        input_.clear();
        output_.clear();

        tile_size_ = 0;

        border_size_ = 0;
        border_budget_ = 0;
        border_overlap_ = 0;

        fill_density_ = 0;
        fill_budget_ = 0;

        h_ = 0;
        v_ = 0;

        seed_ = 0;
        quality_ = -1;

        error_code_ = 0;
    };

    auto parse_int = [&](char* argv, int& i, int& value) -> ParseState { 
        if (value > 0) return ParseState::ERROR;

        std::string value_str = std::string(argv).substr(i); 
        i += value_str.size();

        try { value = std::stoi(value_str); } catch(...) { return ParseState::ERROR; }

        if (value <= 0) return ParseState::ERROR;
        
        return ParseState::START;
    };

    auto parse_string = [&](char* argv, int& i, std::string& s) -> ParseState {
        if (not s.empty()) return ParseState::ERROR;
            
        s = std::string(argv).substr(i);
        i += s.size();

        return ParseState::START;
    };

    /*
    bool res;
    
    ParseTrie<std::function<void(char*,int&, bool& r)>> parse_trie = {
        {"-i", [&](char* argv, int& i, bool& r) { r = parse_string(argv, i, input_) } },
        {"-o", [&](char* argv, int& i, bool& r) { r = parse_string(argv, i, output_) } } 
    }*/

    std::function<ParseState(char*, int&)> transitions[ParseState::END];
    transitions[ParseState::START] =
        [&](char* argv, int& i) -> ParseState {
            char c = argv[i++];
            if (c == '-') return ParseState::START_OP;

            return ParseState::ERROR;
        };
    transitions[ParseState::START_OP] =
        [&](char* argv, int& i) -> ParseState {
            char c = argv[i++];

            if (c == 'i') return ParseState::OP_I;
            if (c == 'o') return ParseState::OP_O;
            if (c == 't') return ParseState::OP_T;
            if (c == 'b') return ParseState::OP_B;
            if (c == 'p') return ParseState::OP_P;
            if (c == 'c') return ParseState::OP_C;
            if (c == 'd') return ParseState::OP_D;
            if (c == 'f') return ParseState::OP_F;
            if (c == 'h') return ParseState::OP_H;
            if (c == 'v') return ParseState::OP_V;
            if (c == 's') return ParseState::OP_S;
            if (c == 'q') return ParseState::OP_Q;

            return ParseState::ERROR;
        };
    transitions[ParseState::OP_I] =
        [&](char* argv, int& i) -> ParseState { 
            return parse_string(argv, i, input_);
        };
    transitions[ParseState::OP_O] =
        [&](char* argv, int& i) -> ParseState { 
            return parse_string(argv, i, output_);
        };
    transitions[ParseState::OP_T] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, tile_size_);
        };
    transitions[ParseState::OP_B] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, border_size_);
        };
    transitions[ParseState::OP_P] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, border_overlap_);
        };
    transitions[ParseState::OP_C] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, border_budget_);
        };
    transitions[ParseState::OP_D] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, fill_density_);
        };
    transitions[ParseState::OP_F] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, fill_budget_);
        }; 
    transitions[ParseState::OP_H] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, h_);
        }; 
    transitions[ParseState::OP_V] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, v_);
        }; 
    transitions[ParseState::OP_S] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, seed_);
        };
    transitions[ParseState::OP_Q] =
        [&](char* argv, int& i) -> ParseState {
            return parse_int(argv, i, quality_);
        };

    auto parse_token = [&](ParseState s, char* argv) -> ParseState {
        int i = 0;
        bool end = false;
        while (argv[i] != '\0' and not end) {
            s = transitions[s](argv, i);
            if (s == ParseState::END or s == ParseState::ERROR) end = true; 
        }
        
        if (s == ParseState::START_OP) s = ParseState::ERROR;
        return s;
    };

    init_vars();

    bool end = false;
    ParseState s = ParseState::START;
    for (int i = 1; i < argc and not end; ++i) {
        s = parse_token(s, argv[i]);
        if (s == ParseState::END or s == ParseState::ERROR) end = true;
    }    
    
    if (input_.empty()) s = ParseState::ERROR;
    if (output_.empty()) output_ = input_ + ".wts.jpg";

    if (tile_size_ <= 0) s = ParseState::ERROR;
    if (border_size_ <= 0) s = ParseState::ERROR;
    if (border_overlap_ <= 0) s = ParseState::ERROR;
    
    if (fill_density_ <= 0) s = ParseState::ERROR;   
    
    if (h_ <= 0) s = ParseState::ERROR;
    if (v_ <= 0) s = ParseState::ERROR;
 
    quality_ = std::min(std::max(0, quality_), DEFAULTS_SIZE - 1);
    if (border_budget_ == 0) border_budget_ = DEFAULT_BORDER_BUDGET[quality_]; 
    if (fill_budget_ == 0) fill_budget_ = DEFAULT_FILL_BUDGET[quality_];
    
    error_code_ = s == ParseState::ERROR;

    return s != ParseState::ERROR;
}

int WangTileGenArgs::error_code() const { return error_code_; }

std::string WangTileGenArgs::input() const { return input_; }
std::string WangTileGenArgs::output() const { return output_; }

int WangTileGenArgs::tile_size() const { return tile_size_; }

int WangTileGenArgs::border_size() const { return border_size_; }
int WangTileGenArgs::border_budget() const { return border_budget_; }
int WangTileGenArgs::border_overlap() const { return border_overlap_; }

int WangTileGenArgs::fill_density() const { return fill_density_; }
int WangTileGenArgs::fill_budget() const { return fill_budget_; }

int WangTileGenArgs::h() const { return h_; }
int WangTileGenArgs::v() const { return v_; }
int WangTileGenArgs::seed() const { return seed_; }

}
