#include <iostream>
#include <thread>
#include <chrono>


#include <WangTileGenArgs.hpp>
#include <WangTileBorderSet.hpp>
#include <WangTileGen.hpp>

#include <OpenImageIO/imageio.h>

bool read_image(const std::string& image_path, wtg::Image& image) {
    auto in = OIIO::ImageInput::open(image_path.data());

    if (not in) return false;

    const OIIO::ImageSpec spec = in->spec();

    std::vector<uint8_t> buffer(spec.height*spec.width*spec.nchannels); 
    bool read = in->read_image(OIIO::TypeDesc::UINT8, buffer.data());

    in->close();
    OIIO::ImageInput::destroy(in);

    if (spec.height < 1 or spec.width < 1 or spec.nchannels > 4) return false;
    if (not read) return false;

    image = wtg::Image(spec.height, spec.width);
   
    uint8_t* image_data = reinterpret_cast<uint8_t*>(image.data().data()); 
    for (int i = 0; i < spec.height*spec.width; ++i) {
        for (int c = 0; c < spec.nchannels; ++c) {
            image_data[i*4 + c] = buffer[i*spec.nchannels + c];
        }
    }

    return true;
}

bool write_image(const std::string& image_path, wtg::Image& image) {
    auto out = OIIO::ImageOutput::create(image_path.data());
    
    if (not out) return false;

    const OIIO::ImageSpec spec(image.width(), image.height(), 4, 
                               OIIO::TypeDesc::UINT8);
    
    bool opened = out->open(image_path.data(), spec);
    bool written = out->write_image(OIIO::TypeDesc::UINT8, 
                                    image.data().data());
    out->close();
    OIIO::ImageOutput::destroy(out);

    return opened and written;
}

void print_usage() {
    std::cout << "Usage:" << std::endl;
    std::cout << "WangTileGen -i input_file " << std::endl;
    std::cout << "            -t tile_size -b border_size -p border_overlap " << std::endl;
    std::cout << "            -v vertical_color_count -h horizontal_color_count " << std::endl;
    std::cout << "            -d fill_density " << std::endl;
    std::cout << "            [-c border_budget] [-f fill_budget] [-q quality] " << std::endl;
    std::cout << "            [-s seed] " << std::endl;
    std::cout << "            [-o output_file]" << std::endl;
}

bool validate_args(const wtg::WangTileGenArgs& args, const wtg::Image src_img) {
    bool is_valid = true;

    int sample_size = wtg::border_sample_size(args.tile_size(), 
                                              args.border_overlap());

    if (not (sample_size < std::min(src_img.width(), src_img.height()) - 2)) {
        std::cout << "ERROR: minimum image dimension should be > ";
        std::cout << sample_size << std:: endl;
        is_valid = false;
    }

    if (not (args.border_overlap() < sample_size)) {
        std::cout << "ERROR: border overlap should be < ";
        std::cout << sample_size << std::endl;
        is_valid = false;
    }  

    return is_valid;
}

int main(int argc, char** argv) {
    
    wtg::WangTileGenArgs args;
    if (not args.Parse(argc, argv)) {
        print_usage();
        return 0;
    }
    
    wtg::Image src_img;
    if (not read_image(args.input(), src_img)) {
        std::cout << "ERROR: Couldn't read image from ";
        std::cout << args.input() << std::endl;
        return -1;
    }

    if (not validate_args(args, src_img)) return -1;

    wtg::WangTileBorderSet border_set(&src_img, args.v(), args.h());

    { 
        std::thread print_border_set_progress([&]() { 

            auto print_message = [&]() -> void {
                std::cout << "Generating tile set borders ... ";
                std::cout << border_set.sets_tested() ;
                std::cout << " sets tested of ";
                std::cout << args.border_budget();
            };

            while (border_set.sets_tested() < args.border_budget()) {
                print_message();
                std::cout << "\r" << std::flush; 
                
                std::this_thread::sleep_for(std::chrono::milliseconds(333));
            }
            
            print_message();
            std::cout << std::endl;
        });

        border_set.GenerateSet(args.tile_size(), 
                               args.border_overlap(), 
                               args.border_size(), 
                               args.border_budget(), 
                               args.seed());

        print_border_set_progress.join();
    }

    wtg::Image tile_pack(args.tile_size()*args.v()*args.v(), 
                         args.tile_size()*args.h()*args.h());
    
    wtg::Matrix<int> border_error_table
        (wtg::error_table_height(args.border_overlap(), 
                                 args.border_size()), 
         args.border_overlap());

    wtg::Matrix<int> fill_error_table
        (wtg::fill_sample_size(args.fill_density(), 
                               args.tile_size(), 
                               args.border_size()), 
                               wtg::fill_overlap(args.border_size()));

    std::array<std::vector<int>, 4> border_paths;
    std::array<std::vector<int>, 4> fill_paths;

    for (auto&& p : border_paths) {
        p = std::vector<int>(border_error_table.height());
    }
    
    for (auto&& p : fill_paths) {
        p = std::vector<int>(fill_error_table.height());
    }

    {
        int tile_count = 0;
        std::thread print_tile_set_progress([&]() { 
            int total_tiles = args.v()*args.v()*args.h()*args.h();
      
            auto print_message = [&]() -> void {
                std::cout << "Generating tiles ... ";
                std::cout << tile_count;
                std::cout << " tiles of "; 
                std::cout << total_tiles;
            }; 
         
            while (tile_count < total_tiles) {
                print_message();
                std::cout << "\r" << std::flush;                

                std::this_thread::sleep_for(std::chrono::milliseconds(333));         
            }
     
            print_message();
            std::cout << std::endl;
        });

        for (int t = 0; t < args.v(); ++t) {
            for (int b = 0; b < args.v(); ++b) {
                for (int l = 0; l < args.h(); ++l) {
                    for (int r = 0; r < args.h(); ++r) {
                        glm::ivec2 tile_offset = 
                            wtg::tile_pack_index(t, b, l, r) * 
                            glm::ivec2(args.tile_size());

                        //tile_offset = glm::ivec2(0,3)*args.tile_size();

                        wtg::set_tile_borders(src_img,
                                              border_set.vertical(t), 
                                              border_set.vertical(b),
                                              border_set.horizontal(l), 
                                              border_set.horizontal(r),
                                              args.tile_size(), 
                                              args.border_overlap(), 
                                              args.border_size(), 
                                              tile_pack, tile_offset,
                                              border_error_table, border_paths);

                        wtg::fill_tile(src_img, 
                                       tile_pack, tile_offset, 
                                       args.fill_density(), args.fill_budget(), 
                                       args.seed(), 
                                       fill_error_table, fill_paths);

                        ++tile_count;
                    }
                }
            }
        }

        print_tile_set_progress.join();
    }

    if (not write_image(args.output(), tile_pack)) {
        std::cout << "ERROR: Couldn't write image at " << args.output() << std::endl;
    }
}
